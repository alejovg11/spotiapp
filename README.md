1. Para la ejecucion de este proyecto primeramente se debe descargar postman.  
2. Una vez descargado y entrado, en el campo de texto de la parte superior donde se indica que es para ingresar una url para hacer una peticion, allí ingrese ésta url: https://accounts.spotify.com/api/token, poniendo la opcion de tipo "POST".
3. En el tab o pestaña que se encuentra seguidamente abajo del campo de texto, dice "Headers", al dirigirse alli apareceran tres columnas llamadas "key", "value" y "description", en la columna de "key" en su primera fila debe poner esto: Content-Type y en la columna "value" en su primera fila debe poner esto: application/x-www-form-urlencoded y en "description" lo dejas asi como esta sin poner nada.
4. En el tab o pestaña llamado body apareceran unas opciones de seleccion, en esto debemos poner la opcion: "x-www-form-urlencode" y seguidamente veremos otra vez tres columnas pero solo nos interesa las dos primeras, en las filas de la columna "key" solo ocuparemos tres y estas son: grant_type, client_id y client_secret, cada una de estas estas iran en cada fila de la columa "key" y finalmente en la columna "value" en sus respectivas filas se debe poner esto: client_credentials, 4ce1308829484af6b2334b6ee8936ac3, 173cf3a62921459a83d43328177cf8b4, cada una de estas iran en cada fila de la columa "value".
5. Hecho ya esto podemos mandar nuestra peticion con el boton azul llamado "send".
6. Abajo puedes observar que se genero algo, donde dice "access_token" tiene como valor un codigo muy largo, este debes copiarlo.
7. Dirigete dentro de este archivo del proyecto: src/app/services/spotify.service.ts, alli encontraras una palabra que dice: "Authorization" seguido de una palabra Bearer y un codigo similar al que vimos anteriormente, este codigo sustituyelo por el que copiaste(solo el codigo, la palabra Bearer se deja tal cual).
8. Guarda los cambios del archivo que hiciste cambios.
8. Una vez hecho esto pasos ya podras seguir con la ejecucion del proyecto que te indico mas abajo.


# Spotiapp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).



