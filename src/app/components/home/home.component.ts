import { Component, OnInit } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  nuevasCanciones: any[] = []
  loading:boolean
  error: boolean
  message:string

  constructor(private spotify: SpotifyService) { 
      this.loading = true
      this.error = false
      this.spotify.getNewReleases()
                  .subscribe((data:any)=>{
                      this.nuevasCanciones = data
                      this.loading = false
                      console.log(data);
                
                  },(errorService)=>{
                    this.loading = false
                    this.error = true
                    this.message = errorService.error.error.message
                    
                  })
    }

}
